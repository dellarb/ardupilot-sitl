#! /bin/bash
#
#curl -sSL https://bitbucket.org/dellarb/ardupilot-sitl/raw/master/sitl_install.sh | sudo bash

function auto_apt_install	{
	if [ $(dpkg-query -W -f='${Status}' $1 2>/dev/null | grep -c "ok installed") -eq 0 ];	then
		apt-get install $1 -y
	fi
}

function auto_delete {
	if [ -e $1 ]; then
		rm $1 -rf
	fi
}

function auto_apt_install {
	install_list="apt-get install -y"
	for var in "$@"
	do
		if [ $var != "auto_apt_install" ]; then
			if [ $(dpkg-query -W -f='${Status}' $var 2>/dev/null | grep -c "ok installed") -eq 0 ];	then
				echo "$var NOT Installed"
				install_list="$install_list "$var
			else
				echo "$var Installed"
			fi
		fi
	done
	if [ "$install_list" != "apt-get install -y" ]; then	#If list has nothing except the apt-get part then nothing to do
		apt-get update
		$install_list		
	fi
}
function auto_pip_install {
	install_list="pip install "
	current_pip_installed=$(pip list --format=columns)
	for var in "$@"
	do
		if [ $var != "auto_pip_install" ]; then
			if [[ ${current_pip_installed,,} != *${var,,}* ]]; then
				echo "$var NOT Installed"
				install_list="$install_list "$var
			else
				echo "$var Installed"
			fi
		fi
	done
	if [ "$install_list" != "pip install " ]; then	#If list has nothing except the pip install part then nothing to do
		$install_list		
	fi
}

################################## Main body starts here

if [ "$(whoami)" != "root" ]; then
	echo "Trying to run install, must be run as root (Prefix with 'sudo')":
	exit 1
fi

chmod -R 777 /etc/ardupilot-sitl

if [ "$1" = "purge" ];	then
	echo "Purge specified, will reset all repositories and run fresh install"
	rm /etc/ardupilot-sitl -rf
	curl -sSL https://bitbucket.org/dellarb/ardupilot-sitl/raw/master/sitl_install.sh | sudo bash
	exit 0
fi

if [ -e "/etc/ardupilot-sitl" ]; then
	echo "Updating ardupilot-sitl scripts..."
	cd /etc/ardupilot-sitl
	git clean -f
	git reset --hard
	git pull origin master
else
	echo "Installing ardupilot-sitl scripts..."
	auto_apt_install git
	mkdir /etc/ardupilot-sitl
	cd /etc/ardupilot-sitl
	git init
	git remote add origin https://bitbucket.org/dellarb/ardupilot-sitl
	git pull origin master
fi

echo "Doing SITL Install..."

echo "Installing apt packages..."
auto_apt_install git python-matplotlib python-serial python-wxtools python-lxml python-scipy python-opencv ccache gawk git python-pip python-pexpect libtool libtool-bin automake autoconf libexpat1-dev screen
echo "Installing pip packages..."
auto_pip_install future pymavlink MAVProxy

#Check for ardupilot and install or update as required
echo "Installing/updating ardupilot..."
if [ -e "/etc/ardupilot-sitl/ardupilot" ]; then
	cd /etc/ardupilot-sitl/ardupilot
	git pull
else
	git clone git://github.com/ArduPilot/ardupilot.git
	cd /etc/ardupilot-sitl/ardupilot
fi
git submodule update --init --recursive

#Check for jsbsim and install or update as required
echo "Installing/updating jsbsim..."
if [ -e "/etc/ardupilot-sitl/jsbsim" ]; then
	cd /etc/ardupilot-sitl/jsbsim
	git pull
else
	git clone git://github.com/tridge/jsbsim.git
	cd /etc/ardupilot-sitl/jsbsim
fi
./autogen.sh --enable-libraries
make

chmod -R 777 /etc/ardupilot-sitl
cp /etc/ardupilot-sitl/sitl_launcher.sh /usr/bin/sitl
chmod +x /usr/bin/sitl

#Install complete running sitl to compile
export PATH=$PATH:/etc/ardupilot-sitl/jsbsim/src
export PATH=$PATH:/etc/ardupilot-sitl/ardupilot/Tools/autotest
export PATH=/usr/lib/ccache:$PATH 
. ~/.bashrc

cd /etc/ardupilot-sitl/ardupilot/ArduPlane
sim_vehicle.py -w

exit 0
