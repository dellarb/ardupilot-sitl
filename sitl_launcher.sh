#! /bin/bash
#

if [ -e "/etc/ardupilot-sitl" ]; then
	#Chmod directory if we are root in case of any permission errors
	if [ "$(whoami)" == "root" ]; then
		chmod -R 777 /etc/ardupilot-sitl
	fi
	#Check for any arguments
	if [ "$1" = "update-purge" ];	then
		bash /etc/ardupilot-sitl/sitl_install.sh purge
	elif [ "$1" = "update" ];	then
		bash /etc/ardupilot-sitl/sitl_install.sh
	fi
	bash /etc/ardupilot-sitl/sitl_run.sh $1 $2 $3 $4 $5
else
	echo "No scripts directory found, trying to run install from web..."
	curl -sSL https://bitbucket.org/dellarb/ardupilot-sitl/raw/master/sitl_install.sh | sudo bash
fi

