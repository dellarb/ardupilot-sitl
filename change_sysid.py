#!/usr/bin/env python

import thread
from time import sleep
from pymavlink import mavutil
import sys

###############################################

#Load arguments into relevent variables
src=sys.argv[1]
new_sysid=sys.argv[2]

#### Start Executiong
#Connect to MAV
print "Connecting to "+src
source_mav = mavutil.mavlink_connection(src)
msg = source_mav.recv_match(type='HEARTBEAT', blocking=True)
print "Heartbeat from APM Sysid:"+str(source_mav.target_system)

i=0
while True:
	msg = source_mav.recv_match(blocking=True)
	if not msg:
		foo=1
	if msg.get_type() == "BAD_DATA":
		if mavutil.all_printable(msg.data):
			sys.stdout.write(msg.data)
			sys.stdout.flush()
	if msg.get_type() == "HEARTBEAT":
		source_mav.mav.param_set_send(source_mav.target_system,0,"SYSID_THISMAV",int(new_sysid),4)
		print "Setting Sysid..."
		i=i+1
		if i == 20:
			print "Shutting down"
			break