##Installation
To install run: "curl -sSL https://bitbucket.org/dellarb/ardupilot-sitl/raw/master/sitl_install.sh | sudo bash"

Once installed you can run by entering "sitl" from the command line
To update enter "sitl update"
To update/purge (replacing all the repositories with a fresh one) enter "sitl update-purge"

##Usage
To run with menu for sitl enter "sitl" in the terminal command line
To run direct without menu enter sitl VEHICLE DESTINATION-IP DESTINATION-PORT LOCATION
For Vehicle use one of "ArduPlane" "ArduCopter" "QuadPlane"
For Location use "Dalby" or anything else included in the locations.txt file

##License
This is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

For more specific details see http://www.gnu.org/licenses, the Quick Guide to GPLv3.

The GNU operating system has an informative GPL FAQ here.

For more info see https://bendellar.com/automatic-software-in-the-loop-sitl-setup-for-ardupilot/