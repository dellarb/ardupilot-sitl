#! /bin/bash

export PATH=$PATH:/etc/ardupilot-sitl/jsbsim/src
export PATH=$PATH:/etc/ardupilot-sitl/ardupilot/Tools/autotest
export PATH=/usr/lib/ccache:$PATH 
. ~/.bashrc

VEHICLE_TYPE=$1
HOSTIP=$2
PORT=$3
LOCATION=$4
INSTANCE=$5
host=$( hostname )

if [ -z "$VEHICLE_TYPE" ]; then
	LASTIP=$(head -n 1 /etc/ardupilot-sitl/sitlconf.txt)
	echo
	echo Script was run without arguments. Entering interactive mode...
	echo
	echo If you want to run without arguments syntax is ./sitl.sh [hostip] [port]
	echo  
	echo
	echo Select Vehicle Type
	echo " 1. ArduPlane"
	echo " 2. ArduCopter"
	echo " 3. QuadPlane"
	read VEHICLE_TYPE
	if [ -z "$VEHICLE_TYPE" ]; then
		VEHICLE_TYPE="ArduPlane"
	elif [ "$VEHICLE_TYPE" = "1" ]; then
		VEHICLE_TYPE="ArduPlane"
	elif [ "$VEHICLE_TYPE" = "2" ]; then
		VEHICLE_TYPE="ArduCopter"
	elif [ "$VEHICLE_TYPE" = "3" ]; then
		VEHICLE_TYPE="QuadPlane"
	fi
	
	echo && echo Enter the IP of terminal to send UDP on port 14550 to. If blank will use last IP: $LASTIP
	read HOSTIP

	if [ -z "$HOSTIP" ]; then
		HOSTIP=$LASTIP
	fi
	echo Using $HOSTIP
	echo
	
	#HOSTIP=dig +short $HOSTIP
	echo $HOSTIP > /etc/ardupilot-sitl/sitlconf.txt

	echo Enter the UDP port to send to. If blank will use 14550
	read PORT

	if [ -z "$PORT" ]; then
		PORT=14550
	fi
	echo Using $PORT
	echo
	
	echo Enter the location to use from locations.txt file. Leave blank to use Dalby
	read LOCATION
fi

if [ -z "$LOCATION" ]; then
		LOCATION="Dalby"
	fi

if [ -z "$PORT" ]; then
	PORT=14550
fi

if [ -z "$INSTANCE" ]; then
	INSTANCE=1
fi

echo
echo Sending stream to $HOSTIP:$PORT location is $LOCATION and instance is $INSTANCE

#if [ "${host:0:4}" = "xSITL" ]; then
#	host=${host:4}
#	sudo screen -d -m -S "SYSID Changer" python /etc/ardupilot-sitl/change_sysid.py udp:127.0.0.1:14551 $host
#	echo "Changing SYSID to $host"
#	sleep 1s
#fi

if [ "$VEHICLE_TYPE" = "QuadPlane" ]; then
	sim_vehicle.py -v ArduPlane -f quadplane -L $LOCATION -I $INSTANCE --out=udp:$HOSTIP:$PORT
else
	sim_vehicle.py -v $VEHICLE_TYPE -L $LOCATION -I $INSTANCE --out=udp:$HOSTIP:$PORT
fi
exit 0





